const colors = {
  bgColor: '#ffffff',
  mainColor: '#ff9933',
  text: '#2F4853',
  yellow: '#FBD671',
  blue: '#12C1E9',
  orange: '#FE816F',
  green: '#55EFC4',
  red: '#FF634D',
};

export default colors;
