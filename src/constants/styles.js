import colors from './colors';

export const PADDING = 20;
export const BORDER_RADIUS = 20;

export const Typo = {
  headline: {
    color: colors.text,
  },
  secondary: {
    color: colors.text,
  },
};

export const MainStyles = {
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.bgColor,
    padding: PADDING,
  },
};
