import colors from './colors';
import server from './server';
import * as styles from './styles';

module.exports = {colors, server, ...styles};
