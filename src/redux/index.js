import {combineReducers} from 'redux';

// Import all reducers
import {reducer as candidates} from './candidates';
import {reducer as alert} from './alert';

// Combine the imported reducers
const AppReducers = combineReducers({
  candidates,
  alert,
});

// Export the combined reducers
export default AppReducers;
