import {store} from '../../PersistGateWrapper';

export const types = {
  SET_CANDIDATES: 'SET_CANDIDATES',
  CLEAN_CANDIDATES: 'CLEAN_CANDIDATES',
};

export const actions = {
  setCandidate: (candidate) =>
    store.dispatch({type: types.SET_CANDIDATES, candidate}),
  cleanCandidate: () => store.dispatch({type: types.CLEAN_CANDIDATES}),
};

const initialState = [];

export const reducer = (state = initialState, action) => {
  const {type, candidate} = action;

  switch (type) {
    case types.SET_CANDIDATES:
      return [...state, candidate];
    case types.CLEAN_CANDIDATES:
      return initialState;
    default:
      return state;
  }
};
