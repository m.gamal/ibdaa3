import {store} from '../../PersistGateWrapper';

export const types = {
  SHOW_ALERT: 'SHOW_ALERT',
  HIDE_ALERT: 'HIDE_ALERT',
};

export const actions = {
  showAlert: (alert) => store.dispatch({type: types.SHOW_ALERT, alert}),
  hideAlert: () => store.dispatch({type: types.HIDE_ALERT}),
};

const initialState = {
  visible: false,
  dismisable: true,
  alertText: '',
  alertDescription: '',
  alertOptions: [{text: 'Close'}],
};

export const reducer = (state = initialState, action) => {
  const {type, alert} = action;

  switch (type) {
    case types.SHOW_ALERT:
      return {...state, ...alert};
    case types.HIDE_ALERT:
      return initialState;
    default:
      return state;
  }
};
