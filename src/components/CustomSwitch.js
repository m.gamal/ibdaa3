import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Animated} from 'react-native';
import {Typo, colors} from '../constants';

const circleSize = 30;
const switchWidthMultiplier = 2;

const CustomSwitch = function ({value, toggle}) {
  const [transformSwitch] = useState(new Animated.Value(17));

  function animateSwitch() {
    Animated.timing(transformSwitch, {
      toValue: value ? -13.5 : 17,
      useNativeDriver: true,
      duration: 100,
    }).start(() => toggle(!value));
  }

  return (
    <TouchableOpacity
      style={styles.buttonContainer}
      activeOpacity={0.8}
      onPress={animateSwitch}>
      <Animated.View
        style={[
          styles.animatedContainer,
          {transform: [{translateX: transformSwitch}]},
        ]}>
        <Text style={styles.text}>{'ON'}</Text>
        <View
          style={[
            styles.circle,
            {backgroundColor: value ? colors.green : colors.red},
          ]}
        />
        <Text style={styles.text}>{'OFF'}</Text>
      </Animated.View>
    </TouchableOpacity>
  );
};

const styles = {
  buttonContainer: {
    width: circleSize * switchWidthMultiplier,
    backgroundColor: '#ccc',
    borderRadius: 50,
    overflow: 'hidden',
  },
  animatedContainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  circle: {
    width: circleSize,
    height: circleSize,
    borderRadius: circleSize / 2,
    elevation: 2,
  },
  text: {
    ...Typo.headline,
    fontSize: 8,
    paddingLeft: 4,
    paddingRight: 6,
  },
};

export default CustomSwitch;
