import {ToastAndroid} from 'react-native';

const Toast = (text = 'Press again to close!', duration = 'SHORT') => {
  ToastAndroid.showWithGravityAndOffset(
    text,
    ToastAndroid[duration],
    ToastAndroid.BOTTOM,
    0,
    250,
  );
};

export default Toast;
