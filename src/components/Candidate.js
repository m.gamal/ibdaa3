import React from 'react';
import {View, Text, Image} from 'react-native';
import {PADDING} from '../constants';

const user = require('../assets/user.png');

const Candidate = ({name, position, date}) => (
  <View style={styles.card}>
    <View style={styles.header}>
      <View style={styles.avatar}>
        <Image source={user} style={{width: '100%', height: '100%'}} />
      </View>
      <Text style={{flex: 1, fontWeight: 'bold'}}>{name}</Text>
    </View>
    <Text style={styles.text}>
      {'Position: '}
      <Text style={{fontWeight: 'normal'}}>{position}</Text>
    </Text>
    <Text style={styles.text}>
      {'Scheduled date: '}
      <Text style={{fontWeight: 'normal'}}>{date}</Text>
    </Text>
  </View>
);

const styles = {
  card: {
    backgroundColor: '#fff',
    width: '85%',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    padding: PADDING,
    marginTop: 15,
    marginBottom: 5,
    elevation: 2,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    padding: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    marginEnd: 12,
    elevation: 2,
  },
  text: {
    fontWeight: 'bold',
  },
};

export default Candidate;
