import React from 'react';
import {TouchableOpacity, Text, ActivityIndicator} from 'react-native';
import {colors, PADDING} from '../constants';

const Button = ({text, style, onPress, fetching, disabled}) => (
  <TouchableOpacity
    disabled={disabled}
    onPress={onPress}
    activeOpacity={0.6}
    style={[styles.buttonStyle, style]}>
    {fetching ? (
      <ActivityIndicator color="#fff" />
    ) : (
      <Text style={{color: '#fff', fontSize: 18}}>{text}</Text>
    )}
  </TouchableOpacity>
);

const styles = {
  buttonStyle: {
    backgroundColor: colors.mainColor,
    width: '85%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    marginVertical: PADDING,
    elevation: 2,
  },
};

export default Button;
