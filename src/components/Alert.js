import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
} from 'react-native';
import {connect} from 'react-redux';
import {MainStyles, BORDER_RADIUS, PADDING, Typo} from '../constants/styles';
import {actions as alertActions} from '../redux/alert';

class Alert extends React.PureComponent {
  renderOptions() {
    const {alert} = this.props;
    if (alert.alertOptions && alert.alertOptions.length) {
      const closeAlert = alert.dismisable
        ? () => alertActions.hideAlert()
        : null;

      return (
        <View style={styles.optionsWrapper}>
          {alert.alertOptions.map((o, i) => (
            <TouchableNativeFeedback
              key={i.toString()}
              onPress={o.onPress || closeAlert}>
              <View
                style={{
                  flex: 1,
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderStartColor: '#F1F9FF',
                  borderStartWidth: i > 0 ? 2 : 0,
                }}>
                <Text
                  style={{...Typo.headline, color: i > 0 ? '#bbb' : '#666'}}>
                  {o.text}
                </Text>
              </View>
            </TouchableNativeFeedback>
          ))}
        </View>
      );
    }
  }

  render() {
    const {alert} = this.props;
    const closeAlert = alert.dismisable ? () => alertActions.hideAlert() : null;
    return (
      <Modal
        transparent
        animationType="fade"
        visible={alert.visible}
        onRequestClose={closeAlert}>
        <TouchableWithoutFeedback onPress={closeAlert}>
          <View style={[styles.container]}>
            <TouchableWithoutFeedback>
              <View style={styles.infoContainer}>
                <View
                  style={{
                    padding: PADDING * 1.5,
                  }}>
                  <Text
                    style={{
                      ...Typo.headline,
                      color: '#444',
                      fontSize: 18,
                      textAlign: 'center',
                    }}>
                    {alert.alertText}
                  </Text>
                  {alert.alertDescription ? (
                    <Text
                      style={{
                        ...Typo.secondary,
                        color: '#888',
                        fontSize: 16,
                        textAlign: 'center',
                        marginTop: 10,
                      }}>
                      {alert.alertDescription}
                    </Text>
                  ) : null}
                </View>
                {this.renderOptions()}
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

const styles = {
  container: {
    ...MainStyles.container,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  infoContainer: {
    backgroundColor: '#fff',
    alignItems: 'center',
    width: '100%',
    borderRadius: BORDER_RADIUS,
    elevation: 2,
    overflow: 'hidden',
  },
  optionsWrapper: {
    flexDirection: 'row',
    width: '100%',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 2,
    borderTopColor: '#F1F9FF',
  },
};

const mapStateToProps = (state) => ({
  alert: state.alert,
});

export default connect(mapStateToProps)(Alert);
