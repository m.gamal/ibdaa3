import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from 'react-native-splash-screen';
// Screens
import Home from '../screens/Home';
import AddCandidate from '../screens/AddCandidate';

const Stack = createStackNavigator();

const AppNavigation = function ({route}) {
  useEffect(() => {
    setTimeout(() => SplashScreen.hide(), 1000);
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName={route}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="AddCandidate" component={AddCandidate} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
