import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
  BackHandler,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import Toast from '../components/Toast';
import Candidate from '../components/Candidate';
import {actions as candidatesActions} from '../redux/candidates';
import {PADDING, colors} from '../constants';

const erase = require('../assets/erase.png');
const plus = require('../assets/plus.png');
const nodata = require('../assets/nodata.png');

const Home = function ({navigation, candidates}) {
  const [exit, confirmExit] = useState(false);

  useEffect(() => {
    const onBackPress = () => {
      if (!exit && navigation.isFocused()) {
        confirmExit(true);
        setTimeout(() => {
          confirmExit(false);
        }, 2000);
        Toast();
        return true;
      }
      return false;
    };

    BackHandler.addEventListener('hardwareBackPress', onBackPress);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', onBackPress);
  });

  function cleanAlert() {
    Alert.alert(
      'Clean candidates!',
      'Are you sure?',
      [
        {text: 'Clean', onPress: () => candidatesActions.cleanCandidate()},
        {text: 'Cancel'},
      ],
      {cancelable: true},
    );
  }

  function noCleanAlert() {
    Alert.alert(
      'No candidates yet.',
      'Add new ones from the add button below.',
      null,
      {cancelable: true},
    );
  }

  function emptyState() {
    return (
      <View style={styles.empty}>
        <Image
          source={nodata}
          style={{width: 200, height: 200}}
          resizeMode="contain"
        />
        <Text style={styles.emptyText}>
          {'No candidates yet.\nAdd new ones from the add button below.'}
        </Text>
      </View>
    );
  }

  return (
    <View style={{flex: 1}}>
      <View style={styles.headerWrapper}>
        <Text style={styles.heading}>{'Candidates'}</Text>
        <TouchableOpacity
          onPress={candidates.length ? cleanAlert : noCleanAlert}
          style={styles.calender}>
          <Image source={erase} style={{width: '75%', height: '75%'}} />
        </TouchableOpacity>
      </View>
      <View style={styles.content}>
        {candidates.length ? (
          <FlatList
            data={candidates}
            showsVerticalScrollIndicator={false}
            keyExtractor={(_, i) => i.toString()}
            contentContainerStyle={{flexGrow: 1, paddingVertical: 10}}
            renderItem={({item}) => <Candidate {...item} />}
          />
        ) : (
          emptyState()
        )}
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={() => navigation.navigate('AddCandidate')}
          style={styles.addButton}>
          <Image source={plus} style={{width: '40%', height: '40%'}} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = {
  headerWrapper: {
    height: 90,
    paddingHorizontal: PADDING,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    elevation: 2,
  },
  heading: {
    color: colors.text,
    fontSize: 22,
    fontWeight: 'bold',
    letterSpacing: 0.5,
  },
  calender: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    width: '100%',
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    margin: PADDING,
    fontWeight: 'bold',
    color: '#999',
    textAlign: 'center',
    lineHeight: 20,
  },
  addButton: {
    width: 60,
    height: 60,
    borderRadius: 25,
    position: 'absolute',
    bottom: 25,
    right: 25,
    elevation: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.mainColor,
  },
};

const mapStateToProps = (state) => ({
  candidates: state.candidates,
});

export default connect(mapStateToProps)(Home);
