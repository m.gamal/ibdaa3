import React, {useState} from 'react';
import {View, Text, TextInput, ScrollView} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {Calendar} from 'react-native-calendars';
import Button from '../components/Button';
import {PADDING, colors} from '../constants';
import {actions as candidatesActions} from '../redux/candidates';
import moment from 'moment';

const positions = [
  'Front-end developer',
  'Back-end developer',
  'Full-stack developer',
  'Data-scientist developer',
];

const AddCandidate = function ({navigation}) {
  const [fetching, toggleFetching] = useState(false);
  const [name, setName] = useState('');
  const [position, setPosition] = useState(positions[0]);
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'));

  function add() {
    toggleFetching(true);
    const candidate = {
      name,
      position,
      date,
    };
    candidatesActions.setCandidate(candidate);

    setTimeout(() => {
      toggleFetching(false);
      navigation.pop();
    }, 1000);
  }

  return (
    <View style={{flex: 1}}>
      <View style={styles.headerWrapper}>
        <Text style={styles.heading}>{'Add Candidate'}</Text>
      </View>
      <ScrollView
        style={styles.content}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{alignItems: 'center', paddingTop: PADDING}}>
        <View style={[styles.input, {paddingHorizontal: 5}]}>
          <Picker
            mode="dropdown"
            style={{height: 44}}
            selectedValue={position}
            onValueChange={(n) => setPosition(n)}>
            {positions.map((p, i) => (
              <Picker.Item key={i} label={p} value={p} />
            ))}
          </Picker>
        </View>
        <TextInput
          value={name}
          maxLength={50}
          onChangeText={(n) => setName(n)}
          placeholder="Candidate name"
          placeholderTextColor="#aaa"
          selectionColor={colors.mainColor}
          style={styles.input}
        />
        <View style={{width: '85%', marginVertical: 10}}>
          <Calendar
            onDayPress={({dateString}) => setDate(dateString)}
            style={{
              height: 320,
              borderRadius: 14,
              overflow: 'hidden',
              paddingBottom: PADDING,
              elevation: 2,
            }}
            markedDates={{[date]: {selected: true}}}
            markingType="custom"
            theme={{
              arrowColor: colors.mainColor,
              selectedDayBackgroundColor: colors.green,
            }}
          />
        </View>
      </ScrollView>
      <Button
        text="Add Candidate"
        style={{backgroundColor: !name ? '#aaa' : colors.mainColor}}
        onPress={add}
        fetching={fetching}
        disabled={fetching || !name}
      />
    </View>
  );
};

const styles = {
  headerWrapper: {
    height: 90,
    paddingHorizontal: PADDING,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    elevation: 2,
  },
  heading: {
    color: colors.text,
    fontSize: 22,
    fontWeight: 'bold',
    letterSpacing: 0.5,
  },
  content: {
    flex: 1,
    width: '100%',
  },
  input: {
    width: '85%',
    height: 46,
    borderColor: colors.mainColor,
    borderWidth: 0.8,
    borderRadius: 10,
    marginVertical: 10,
    paddingHorizontal: 15,
  },
};

export default AddCandidate;
