import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import AppNavigation from './src/navigation/AppNavigation';

const App = function () {
  return (
    <>
      <StatusBar hidden />
      <SafeAreaView style={{flex: 1}}>
        <AppNavigation />
      </SafeAreaView>
    </>
  );
};

export default App;
