import {AppRegistry} from 'react-native';
import PersistGateWrapper from './PersistGateWrapper';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => PersistGateWrapper);
