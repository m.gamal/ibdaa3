import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/es/integration/react';
import App from './App';
import configureStore from './src/redux/configureStore';

const {persistor, store} = configureStore();
export {store};

class PersistGateWrapper extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
      </Provider>
    );
  }
}

export default PersistGateWrapper;
